import { Component, OnInit } from '@angular/core';
import { Utilisateur as User} from '../../../model/user.model';
import { NgForm } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../../providers/user/user.service';
import { ToasterService } from 'angular2-toaster';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss'],
  animations: [routerTransition()]
})
export class UserEditComponent implements OnInit {
  user:User=new User();
  constructor(public activeModal: NgbActiveModal, private userService: UserService, private toasterService: ToasterService) { }

  ngOnInit() {
  }
  
  create(updateForm: NgForm) {
    this.user.username = updateForm.form.value.username;
    this.user.password = updateForm.form.value.password;
    this.userService.add(this.user).subscribe(data => {
      this.toasterService.pop('success', 'Utilisateur', 'Information mise à jour avec succès');
      this.activeModal.close();
    }, err => {
      alert('Impossible d\'ajouter ces informations');
    });
  }

}
