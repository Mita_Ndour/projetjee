import { Component, OnInit } from '@angular/core';
import { Utilisateur as User } from '../../../model/user.model';
import { UserService } from '../../../providers/user/user.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss'],
  animations: [routerTransition()]
})
export class UserAddComponent implements OnInit {

  user: User = new User();
  constructor(public activeModal: NgbActiveModal, private userService: UserService, private toasterService: ToasterService) {
  }

  ngOnInit() {
  }
  create(updateForm: NgForm) {
    this.user.username = updateForm.form.value.username;
    this.user.password = updateForm.form.value.password;
    this.userService.add(this.user).subscribe(data => {
      this.toasterService.pop('success', 'Utilisateur', 'Un nouvel utilisateur a été ajouté avec succès');
      this.activeModal.close();
    }, err => {
      alert('Impossible d\'ajouter ces informations');
    });
  }

}
