import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuPhotoComponent } from './menu-photo.component';

describe('MenuPhotoComponent', () => {
  let component: MenuPhotoComponent;
  let fixture: ComponentFixture<MenuPhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuPhotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
