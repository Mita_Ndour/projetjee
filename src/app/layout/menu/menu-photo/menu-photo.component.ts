import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-menu-photo',
  templateUrl: './menu-photo.component.html',
  styleUrls: ['./menu-photo.component.scss'],
  animations: [routerTransition()]
})
export class MenuPhotoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
