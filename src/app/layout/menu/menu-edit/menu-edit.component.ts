import { Component, OnInit } from '@angular/core';
import { Menu } from '../../../model/menu.model';
import { TypePlat } from '../../../model/type-plat.model';
import { NgForm } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-menu-edit',
  templateUrl: './menu-edit.component.html',
  styleUrls: ['./menu-edit.component.scss'],
  animations: [routerTransition()]
})
export class MenuEditComponent implements OnInit {
  menu:Menu=new Menu();
  typeplats:TypePlat[]=[{id:"1",createdAt:"12",nom:"Africain"},
  {id:"2",createdAt:"12",nom:"Marocain"},
  {id:"3",createdAt:"12",nom:"Bresilien"}];
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }
  update(updateForm: NgForm) {
    
    this.activeModal.close('Close click');
  }

}
