import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './menu.component';
import { MenuAddComponent } from './menu-add/menu-add.component';
import { MenuEditComponent } from './menu-edit/menu-edit.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { MenuRoutingModule } from './menu-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule, NgbModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { TemplateModule } from '../../template/template.module';
import { MenuInfoComponent } from './menu-info/menu-info.component';
import { MenuPhotoComponent } from './menu-photo/menu-photo.component';
import { ToasterService } from 'angular2-toaster';
import { MenuService } from '../../providers/menu/menu.service';

@NgModule({
  imports: [
    CommonModule,MenuRoutingModule,
    TranslateModule,
    NgbDropdownModule.forRoot(),
    FormsModule, NgbModule.forRoot(), NgbModalModule, TemplateModule
  ],
  providers:[ToasterService,MenuService],
  declarations: [MenuComponent, MenuAddComponent, MenuEditComponent, MenuListComponent, MenuInfoComponent, MenuPhotoComponent]
})
export class MenuModule { }
