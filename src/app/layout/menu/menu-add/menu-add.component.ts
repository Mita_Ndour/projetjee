import { Component, OnInit } from '@angular/core';
import { Menu } from '../../../model/menu.model';
import { MenuService } from '../../../providers/menu/menu.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToasterService } from 'angular2-toaster';
import { NgForm } from '@angular/forms';
import { TypePlat } from '../../../model/type-plat.model';
import { routerTransition } from '../../../router.animations';
import { TypePlatService } from '../../../providers/type-plat/type-plat.service';
import { ImageService } from '../../../providers/image/image.service';

@Component({
  selector: 'app-menu-add',
  templateUrl: './menu-add.component.html',
  styleUrls: ['./menu-add.component.scss'],
  animations: [routerTransition()]
})
export class MenuAddComponent implements OnInit {
  menu:Menu=new Menu();
  this_this:any;
  typeplat:any;
  typeplats:TypePlat[]=[{id:"1",createdAt:"12",nom:"Africain"},
  {id:"2",createdAt:"12",nom:"Marocain"},
  {id:"3",createdAt:"12",nom:"Bresilien"}];
  file:any;
  files:File;
  fi:any;
  constructor(public activeModal: NgbActiveModal,private typeplatService:TypePlatService,
    private imageService:ImageService, private menuservice: MenuService,private toasterService: ToasterService) { }

  ngOnInit() {
    this.getTypePlat();
  }
  add(addForm: NgForm) {
    console.log(addForm.value);

    if(this.files!=null){
      this.imageService.add(this.fi).subscribe(data=>{console.log(data);},err=>console.log(err));
    }
    this.menuservice.add(this.menu).subscribe(data=>{
      this.this_this.get();
      this.activeModal.close('Close click');
    },
      err=>this.toasterService.pop("error")
      );
  }
  getTypePlat(){
    this.typeplatService.getAll().subscribe(data=>this.typeplats=data as TypePlat[],
      err=>this.toasterService.pop("error","erreur","erreur inconnu"))
  }
  onFileChanged(event) {
    if(event.target.files && event.target.files[0]){
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) =>{
        this.fi = event.target;
        console.log(event.target);
      }
      this.files=this.fi.result;
    }
    else
    this.files=null;
  }
}
