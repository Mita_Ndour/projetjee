import { Component, OnInit } from '@angular/core';
import { Menu } from '../../../model/menu.model';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MsgConfirmationComponent } from '../../../template/msg-confirmation/msg-confirmation.component';
import { MenuEditComponent } from '../menu-edit/menu-edit.component';
import { routerTransition } from '../../../router.animations';
import { MenuService } from '../../../providers/menu/menu.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-info',
  templateUrl: './menu-info.component.html',
  styleUrls: ['./menu-info.component.scss'],
  animations: [routerTransition()]
})
export class MenuInfoComponent implements OnInit {
  menu:Menu={id:"1",createdAt:JSON.stringify(new Date()),
  desc:"description",photo:"fo",plat:"thie",
  prix:100,typeplat:{id:"1",createdAt:"12",nom:"nom"}};
  constructor(private modalService: NgbModal,private menuService:MenuService,private Route:Router) { }

  ngOnInit() {
  }
  open()
  {
    const modal: NgbModalRef = this.modalService.open(MenuEditComponent);
    (<MenuEditComponent>modal.componentInstance).menu = this.menu;
  }
  delete(){
    const modal: NgbModalRef = this.modalService.open(MsgConfirmationComponent);
    (<MsgConfirmationComponent>modal.componentInstance).this_this = this;
    (<MsgConfirmationComponent>modal.componentInstance).titre = "Suppression de menu";
    (<MsgConfirmationComponent>modal.componentInstance).message = "Voulez-vous supprimer ce menu ?";
  }
  action(){
    this.menuService.delete(this.menu.id).subscribe(data=>{
      this.Route.navigate(['menus']);
      alert("supprimer avec succès");
    },err=>alert("Erreur"));
    
    
  }

}
