import { Component, OnInit, Input } from '@angular/core';
import { TypePlat } from '../../../model/type-plat.model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TypePlatService } from '../../../providers/type-plat/type-plat.service';
import { NgForm } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-type-plat-edit',
  templateUrl: './type-plat-edit.component.html',
  styleUrls: ['./type-plat-edit.component.scss'],
  animations: [routerTransition()]
})
export class TypePlatEditComponent implements OnInit {
  typeplat: TypePlat = new TypePlat();
  constructor(public activeModal: NgbActiveModal, private typeplatService: TypePlatService, private toasterService: ToasterService) {
  }

  ngOnInit() {
  }
  update(updateForm: NgForm) {
    this.typeplat.nom = updateForm.form.value.nom;
    this.typeplatService.update(this.typeplat.id,this.typeplat).subscribe(data => {
      this.toasterService.pop('success', 'Type Plat', 'Information mis à jour avec succès');
      this.activeModal.close();
    }, err => {
      this.toasterService.pop('error', 'Type Plat', 'Impossible de mettre à jour ces informations');
    });
  }

}
