import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypePlatAddComponent } from './type-plat-add.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToasterService } from 'angular2-toaster';
import { TypePlatService } from '../../../providers/type-plat/type-plat.service';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('TypePlatAddComponent', () => {
  let component: TypePlatAddComponent;
  let fixture: ComponentFixture<TypePlatAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[BrowserAnimationsModule,FormsModule,
        TranslateModule.forRoot(),HttpClientTestingModule],
      declarations: [ TypePlatAddComponent ],
      providers:[NgbActiveModal, ToasterService,TypePlatService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypePlatAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
