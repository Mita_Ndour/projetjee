import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { TypePlatService } from '../../../providers/type-plat/type-plat.service';
import { Router } from '@angular/router';
import { TypePlat } from '../../../model/type-plat.model';
import { ToasterService } from 'angular2-toaster';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MsgConfirmationComponent } from '../../../template/msg-confirmation/msg-confirmation.component';
import { TypePlatEditComponent } from '../type-plat-edit/type-plat-edit.component';
import { TypePlatAddComponent } from '../type-plat-add/type-plat-add.component';
import { SessionService } from '../../../providers/session/session.service';
declare var $;
@Component({
  selector: 'app-type-plat-list',
  templateUrl: './type-plat-list.component.html',
  styleUrls: ['./type-plat-list.component.scss'],
  animations: [routerTransition()]
})
export class TypePlatListComponent implements OnInit {

  constructor(private typeplatService:TypePlatService,
    private route:Router,
    private toasterService:ToasterService, 
    private modalService: NgbModal,private sessionService:SessionService) { }
  @ViewChild('dataTable') table;
  dataTable: any;
  dtOptions: any;
  add=true;
  title = 'Une liste';
  data: TypePlat[] = [{id:"1",createdAt:"12",nom:"Africain"},
  {id:"2",createdAt:"12",nom:"Marocain"},
  {id:"3",createdAt:"12",nom:"Bresilien"}];
  dataTableResult: any;
  ngOnInit(): void {
    this.get();
    this.initTable();
  }

  get() {
    this.typeplatService.getAll().subscribe(data=>{
      this.data = data;
     this.dataTable.DataTable().clear();
     this.dataTable.DataTable().rows.add(data);
     this.dataTable.DataTable().draw();
    },
    err=>this.toasterService.pop("error","Liste","Erreur inattendue"));
  }
  initTable(){
    this.dataTable = $(this.table.nativeElement);
    this.dataTableResult = this.dataTable.DataTable({
      data: this.data,
      columns: [
        {
          data: 'nom',
          render: function (data, type, full) {
              return ` <a class="cursor-pointer">` + data + `</a>`;
          }
        },
        { data: 'createdAt',
        render:  function(data, type, full) {
          //const date = `${data.values[2]}-${data.values[1]}-${data.values[0]} ${data.hourOfDay}:${data.minuteOfHour}`;
          return data;
        }
      },
      { data: 'id',
      render:  function(data, type, full) {
        return ` <button class="btn btn-danger btn-xs">supprimer</button>`;
      }
    }
      ],
      language: this.sessionService.getlanguetable(),
      'pagingType': 'full',
      'lengthChange': false,
      'searching': true,
      pageLength: 20,
      dom: 'Bfrtip',
      buttons: [
        {
          extend: 'print',
        },
        {
          extend: 'excel',
        }
      ]
    });
    this.redirection(this.dataTableResult);
  }
  
  public redirection(table) {
    const th =this;
    this.dataTable.on('click', 'td', function() {
      const cellData: {row: string, column: string} = table.cell(this)[0][0];
      const data:TypePlat = table.data()[cellData.row];
      if(cellData.column=="2"){
        th.delete(data.id);
      }
      else
      th.edit(data);
    });
  }
  open(){
    this.modalService.open(TypePlatAddComponent);
  }

  public delete(id:string){
    const modal: NgbModalRef = this.modalService.open(MsgConfirmationComponent);
    (<MsgConfirmationComponent>modal.componentInstance).this_this = this;
    (<MsgConfirmationComponent>modal.componentInstance).data = id;
    (<MsgConfirmationComponent>modal.componentInstance).titre = "Suppression de type de plat";
    (<MsgConfirmationComponent>modal.componentInstance).message = "Voulez-vous supprimer ce type de plat ?"+id;
  }
  public action(id:string){
    this.typeplatService.delete(id).subscribe(data=>
      this.toasterService.pop("success","Suppression utilisateur","Utilisateur supprimer avec succès"),
      err=>this.toasterService.pop("error","Echec","Impossible de supprimer, erreur inattendue"));
  }
  
  public edit(data:TypePlat){
    const modal: NgbModalRef = this.modalService.open(TypePlatEditComponent);
    (<TypePlatEditComponent>modal.componentInstance).typeplat = data;
  }
}
