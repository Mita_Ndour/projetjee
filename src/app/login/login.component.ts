import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { SessionService } from '../providers/session/session.service';
import { UserService } from '../providers/user/user.service';
import { Utilisateur as User } from '../model/user.model';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    constructor(private sessionService:SessionService,private userService:UserService) {}

    ngOnInit() {}

    onLoggedin() {
        let user:User=new User();
        user.username = "DAO Hamadou";
        this.userService.login(user).subscribe(data=>{
            this.sessionService.login(data);
        },
        err=>{
            alert("Erreur du serveur");
            this.sessionService.login(user);
          });

    }
}
