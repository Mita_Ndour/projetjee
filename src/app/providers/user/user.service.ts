import { Injectable } from '@angular/core';
import { Utilisateur as User } from '../../model/user.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class UserService {
  private baseUrl = '/api/utilisateur';

  constructor(private http: HttpClient) { }

  getAll():Observable<any> {
    return this.http.get(this.baseUrl);
  }
  add(user:User): Observable<any>{
    return this.http.post(this.baseUrl,user);
  }
  update(id:string,user:User): Observable<any>{
    return this.http.put(this.baseUrl+"/"+id,user);
  }
  getById(id:string): Observable<any>{
    return this.http.get(this.baseUrl+"/"+id);
  }
  delete(id:string): Observable<any>{
    return this.http.delete(this.baseUrl+"/"+id);
  }
  login(user:User): Observable<any> {
    return this.http.post(`/api/login`, { login: user.username, password: user.password });
  }
  logout(): Observable<any> {
    return this.http.post('/api/logout', {});
  }
}